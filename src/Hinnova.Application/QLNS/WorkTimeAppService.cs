﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Dapper;
using Hinnova.Authorization.Users;
using Hinnova.Authorization.Users.Dto;
using Hinnova.Configuration;
using Hinnova.QLNS.Dtos;
using Hinnova.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Hinnova.QLNS
{
    public class WorkTimeAppService : HinnovaAppServiceBase, IWorkTimeAppService
    {
        private readonly IRepository<HoSo, int> _hoSoRepository;

        private readonly UserManager _userManager;

        private readonly IRepository<HrWorkTime> _hrWorkTimeRepository;

        private readonly IStorageService _storageService;

        private readonly string _connectionString;

        public WorkTimeAppService(IRepository<HoSo, int> hoSoRepository, IWebHostEnvironment env, UserManager userManager, IRepository<HrWorkTime> hrWorkTimeRepository, IStorageService storageService)
        {
            _hoSoRepository = hoSoRepository;
            _userManager = userManager;
            _storageService = storageService;
            _hrWorkTimeRepository = hrWorkTimeRepository;
            _connectionString = env.GetAppConfiguration().GetConnectionString("Default");
        }

        #region Mobile

        public async Task<ListDangKyInputMobileDto> GetQuyTrinhDuyet(int branchId)
        {

            var conn = new SqlConnection(_connectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            var parameters = new DynamicParameters();
            parameters.Add("@branchId", branchId);
            var lstUserListDto = (await conn.QueryAsync<UserListDto>("GetUserByParentFather", parameters, null, null, CommandType.StoredProcedure)).ToList();
            var result = new ListDangKyInputMobileDto();
            if (!lstUserListDto.Any())
                return null;
            lstUserListDto.ForEach(_ =>
            {
                switch (_.BranchType)
                {
                    case "TN":
                        {
                            result.TruongNhom.Id = _.Id.ToString();
                            result.TruongNhom.MaChamCong = _.MaChamCong;
                            result.TruongNhom.Name = _.HoVaTen;
                            result.TruongNhom.Username = _.UserName;
                            break;
                        }

                    case "TP":
                        {
                            result.TruongPhong.Id = _.Id.ToString();
                            result.TruongPhong.MaChamCong = _.MaChamCong;
                            result.TruongPhong.Name = _.HoVaTen;
                            result.TruongPhong.Username = _.UserName;
                            break;
                        }
                    case "GDK":
                        {
                            result.GiamDocKhoi.Id = _.Id.ToString();
                            result.GiamDocKhoi.MaChamCong = _.MaChamCong;
                            result.GiamDocKhoi.Name = _.HoVaTen;
                            result.GiamDocKhoi.Username = _.UserName;
                            break;
                        }
                    case "TCNS":
                        {
                            result.Tcns.Id = _.Id.ToString();
                            result.Tcns.MaChamCong = _.MaChamCong;
                            result.Tcns.Name = _.HoVaTen;
                            result.Tcns.Username = _.UserName;
                            break;
                        }
                    case "BGDDH":
                        {
                            result.GiamDocDieuHanh.Id = _.Id.ToString();
                            result.GiamDocDieuHanh.MaChamCong = _.MaChamCong;
                            result.GiamDocDieuHanh.Name = _.HoVaTen;
                            result.GiamDocDieuHanh.Username = _.UserName;
                            break;
                        }
                }
            });
            return result;
        }

        public async Task<HrWorkTimeDto> GetWorkTimeDetail(int id, string receiveId) 
            => (await _hrWorkTimeRepository.GetAllListAsync(_ => _.Id == id && _.ApproverId == int.Parse(receiveId)))
                .Join(_userManager.Users, _ => _.CreatorUserId, _ => _.Id, (_, __) => new { _, __ })
                .Join(await _hoSoRepository.GetAllListAsync(), _ => _.__.EmployeeCode, _ => _.MaHoSo, (_, __) 
                    => new HrWorkTimeDto
                        {
                            Id = _._.Id,
                            CreatorUserId = __.Id,
                            Status = _._.Status,
                            TenCty = __.TenCty,
                            HoVaTen = __.HoVaTen,
                            Email = _.__.EmailAddress,
                            Image = __.AnhDaiDien,
                            Attachment = _storageService.GetFileUrl(_._.Attachment),
                            DocumentType = _._.DocumentType,
                            ApproverId = _._.ApproverId,
                            Notes = _._.Notes,
                            Reasons = _._.Reasons,
                            TimeFrom = _._.TimeFrom.ToString(CultureInfo.CurrentCulture),
                            TimeTo = _._.TimeTo.ToString(CultureInfo.CurrentCulture),
                            TotalMinutes = _._.TotalMinutes,
                        })
                .FirstOrDefault();

        public async Task<List<HrWorkTimeDto>> GetWorkTimeUnCheck(string userId) 
            => (await _hrWorkTimeRepository.GetAllListAsync(_ =>
                _.ApproverId == int.Parse(userId) && !_.IsDeleted && _.Status != 1)).OrderByDescending(_ => _.CreationTime)
                .Join(_userManager.Users, _ => _.CreatorUserId, _ => _.Id, (_, __) => new { _, __ })
                .Join(await _hoSoRepository.GetAllListAsync(), _ => _.__.EmployeeCode, _ => _.MaHoSo, (_, __)
                    => new HrWorkTimeDto
                    {
                        Id = _._.Id,
                        CreatorUserId = __.Id,
                        Status = _._.Status,
                        TenCty = __.TenCty,
                        HoVaTen = __.HoVaTen,
                        Email = _.__.EmailAddress,
                        Image = __.AnhDaiDien,
                        Attachment = _storageService.GetFileUrl(_._.Attachment),
                        DocumentType = _._.DocumentType,
                        ApproverId = _._.ApproverId,
                        Notes = _._.Notes,
                        Reasons = _._.Reasons,
                        TimeFrom = _._.TimeFrom.ToString(CultureInfo.CurrentCulture),
                        TimeTo = _._.TimeTo.ToString(CultureInfo.CurrentCulture),
                        TotalMinutes = _._.TotalMinutes,
                    }).ToList();

        #endregion

        #region Method Supports

        private double MathTimeViolatingRuleDurationFirst(DateTime processDate, string timeCheck, TruongGiaoDich workTimeMonday, TruongGiaoDich workTimeDaily, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (IsWorkStartMorning(timeCheckList, middleStart))
            {
                switch (processDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (workTimeMonday != null && timeCheckList.Any())
                        {
                            double minutesFirst = (timeCheckList.First() - workTimeMonday.Value.ToTimeSpan()).TotalMinutes;
                            result = minutesFirst > 0 ? minutesFirst : 0;
                        }
                        break;

                    default:
                        if (workTimeDaily != null && timeCheckList.Any())
                        {
                            double minutesFirst = (timeCheckList.First() - workTimeDaily.Value.ToTimeSpan()).TotalMinutes;
                            result = minutesFirst > 0 ? minutesFirst : 0;
                        }
                        break;
                }
            }
            if (IsWorkStartAfternoon(timeCheckList, middleStart, middleEnd))
            {
                if (middleEnd != null && timeCheckList.Any())
                {
                    double minutesFirst = (timeCheckList.First() - middleEnd.Value.ToTimeSpan()).TotalMinutes;
                    result = minutesFirst > 0 ? minutesFirst : 0;
                }
            }
            return result;
        }

        private double MathTimeViolatingRuleDurationLast(string timeCheck, TruongGiaoDich middleEnd, TruongGiaoDich workTimeEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (workTimeEnd != null && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkAfternoon(timeCheckList, middleEnd))
            {
                double minutesLast = (workTimeEnd.Value.ToTimeSpan() - timeCheckList.Last()).TotalMinutes;
                result = minutesLast > 0 ? minutesLast : 0;
            }
            return result;
        }

        private double MathTimeOTDurationDaily(string timeCheck, TruongGiaoDich workTimeEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (workTimeEnd != null && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5)
            {
                double minutesLast = (timeCheckList.Last() - workTimeEnd.Value.ToTimeSpan()).TotalMinutes;
                result += minutesLast > 0 ? minutesLast : 0;
            }
            return result;
        }

        private double MathTimeWorkMorningDuration(string timeCheck, DateTime processDate,
            TruongGiaoDich workTimeMonday, TruongGiaoDich workTimeDaily,
            TruongGiaoDich workTimeApprove, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (middleStart != null && middleEnd != null && workTimeApprove != null
                && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkStartMorning(timeCheckList, middleStart))
            {
                if (processDate.DayOfWeek == DayOfWeek.Monday)
                {
                    var workStart = timeCheckList.First() >= workTimeMonday.Value.ToTimeSpan() ? timeCheckList.First() : workTimeMonday.Value.ToTimeSpan();
                    var workEnd = timeCheckList.Last() >= middleStart.Value.ToTimeSpan() ? middleStart.Value.ToTimeSpan() : timeCheckList.Last();
                    var totalMorning = (workEnd - workStart).TotalMinutes;
                    result = totalMorning > 0 && totalMorning > Convert.ToDouble(workTimeApprove.Value) ? totalMorning : 0;
                }
                else
                {
                    var workStart = timeCheckList.First() >= workTimeDaily.Value.ToTimeSpan() ? timeCheckList.First() : workTimeDaily.Value.ToTimeSpan();
                    var workEnd = timeCheckList.Last() >= middleStart.Value.ToTimeSpan() ? middleStart.Value.ToTimeSpan() : timeCheckList.Last();
                    var totalMorning = (workEnd - workStart).TotalMinutes;
                    result = totalMorning > 0 && totalMorning > Convert.ToDouble(workTimeApprove.Value) ? totalMorning : 0;
                }
            }
            return result;
        }

        private double MathTimeWorkAfternoonDuration(string timeCheck, TruongGiaoDich workTimeEnd,
            TruongGiaoDich workTimeApprove, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (middleStart != null && middleEnd != null && workTimeApprove != null && workTimeEnd != null
                && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkAfternoon(timeCheckList, middleEnd))
            {
                var workStart = timeCheckList.First() >= middleEnd.Value.ToTimeSpan() ? timeCheckList.First() : middleEnd.Value.ToTimeSpan();
                var workEnd = timeCheckList.Last() <= workTimeEnd.Value.ToTimeSpan() ? timeCheckList.Last() : workTimeEnd.Value.ToTimeSpan();
                var totalAfternoon = (workEnd - workStart).TotalMinutes;
                result = totalAfternoon > 0 && totalAfternoon > Convert.ToDouble(workTimeApprove.Value) ? totalAfternoon : 0;
            }

            return result;
        }

        private bool IsWorkStartMorning(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleStart)
        {
            return timeCheckList.First() < middleStart.Value.ToTimeSpan();
        }

        private bool IsWorkStartAfternoon(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            return timeCheckList.First() > middleStart.Value.ToTimeSpan() && timeCheckList.First() < middleEnd.Value.ToTimeSpan();
        }

        private bool IsWorkAfternoon(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleEnd)
        {
            return timeCheckList.Last() > middleEnd.Value.ToTimeSpan();
        }

        #endregion Method Supports
    }
}
