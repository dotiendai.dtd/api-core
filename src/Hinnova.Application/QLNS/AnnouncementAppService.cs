﻿using Abp.Domain.Repositories;
using Dapper;
using Hinnova.Configuration;
using Hinnova.QLNS.Dtos;
using Microsoft.AspNetCore.Hosting;

using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Hinnova.QLNS
{
    public class AnnouncementAppService : HinnovaAppServiceBase, IAnnouncementAppService
    {
        private readonly string _connectionString;

        private readonly IRepository<AnnouncementUser> _announcementUserRepository;

        private readonly IRepository<Announcement, Guid> _announcementRepository;

        public AnnouncementAppService(IWebHostEnvironment env, IRepository<AnnouncementUser> announcementUserRepository, IRepository<Announcement, Guid> announcementRepository)
        {
            _announcementUserRepository = announcementUserRepository;
            _announcementRepository = announcementRepository;
            _connectionString = env.GetAppConfiguration().GetConnectionString("Default");
        }

        #region Mobile

        public async Task<PagedResult<AnnouncementDto>> GetAllUnReadPaging(long userId, int pageIndex, int pageSize)
        {
            var conn = new SqlConnection(_connectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            var _ = new DynamicParameters();
            _.Add("@pageIndex", pageIndex);
            _.Add("@pageSize", pageSize);
            _.Add("@userId", userId);
            _.Add("@totalRow", dbType: DbType.Int32, direction: ParameterDirection.Output);
            var result = await conn.QueryAsync<AnnouncementDto>("AnnouncementPaging", _, null, null, CommandType.StoredProcedure);
            var totalRow = _.Get<int>("@totalRow");
            return new PagedResult<AnnouncementDto>
            {
                Items = result.ToList(),
                TotalRow = totalRow,
                PageIndex = pageIndex,
                PageSize = pageSize
            };
        }

        public async Task<List<AnnouncementDto>> GetAllUnRead(long userId) =>
            (from _ in await _announcementRepository.GetAllListAsync()
             join __ in await _announcementUserRepository.GetAllListAsync()
                 on _.Id equals __.AnnouncementId
                 into ___
             from ____ in ___.DefaultIfEmpty()
             where ____.HasRead == false && (____.UserId == null || ____.UserId == userId)
             select _).OrderByDescending(_ => _.DateCreated).Select(_ => new AnnouncementDto
             {
                 Status = _.Status,
                 UserId = _.UserId,
                 DateCreated = _.DateCreated,
                 Title = _.Title,
                 Content = _.Content,
                 DateModified = _.DateModified,
                 Id = _.Id,
                 Image = _.Image,
                 EntityId = _.EntityId,
                 EntityType = _.EntityType
             }).ToList();

        public async Task<bool> MarkAsRead(List<AnnouncementUserDto> lstAnnouncementUserDtos)
        {
            var result = false;
            foreach (var announcementUserDto in lstAnnouncementUserDtos)
            {
                var announce = await _announcementUserRepository.FirstOrDefaultAsync(_ => _.AnnouncementId == announcementUserDto.AnnouncementId && _.UserId == announcementUserDto.UserId);
                if (announce != null)
                {
                    if (announce.HasRead == false)
                    {
                        announce.HasRead = true;
                        await _announcementUserRepository.UpdateAsync(announce);
                        result = true;
                    }
                }
            }

            return result;
        }

        public async Task<AnnouncementDto> GetDetailAnnouncement(Guid id, long receiveId) => (await _announcementRepository.GetAllListAsync())
                   .Join((await _announcementUserRepository.GetAllListAsync()), _ => _.Id, _ => _.AnnouncementId, (_, __) => new { _, __ })
                   .Where(_ => _._.Id.Equals(id) && _.__.UserId == receiveId).Select(_ => new AnnouncementDto
                   {
                       Id = _._.Id,
                       Title = _._.Title,
                       Content = _._.Content,
                       Image = _._.Image,
                       UserId = _._.UserId,
                       DateCreated = _._.DateCreated,
                       DateModified = _._.DateModified,
                       Status = _._.Status,
                       EntityType = _._.EntityType,
                       EntityId = _._.EntityId
                   }).FirstOrDefault();


        #endregion Mobile
    }
}