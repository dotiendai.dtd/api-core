﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Organizations;
using Abp.UI;
using Dapper;
using Hinnova.Authorization;
using Hinnova.Authorization.Users;
using Hinnova.Authorization.Users.Dto;
using Hinnova.Configuration;
using Hinnova.Dto;
using Hinnova.QLNS.Dtos;
using Hinnova.QLNS.Exporting;
using Hinnova.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Hinnova.QLNSDtos;

namespace Hinnova.QLNS
{
    /// [AbpAuthorize(AppPermissions.Pages_QuyTrinhCongTacs)]
    public class QuyTrinhCongTacsAppService : HinnovaAppServiceBase, IQuyTrinhCongTacsAppService
    {
        private readonly IRepository<QuyTrinhCongTac> _quyTrinhCongTacRepository;

        private readonly IQuyTrinhCongTacsExcelExporter _quyTrinhCongTacsExcelExporter;

        private readonly IRepository<TruongGiaoDich> _truongGiaoDichRepository;

        private readonly IViewRenderAppService _viewRenderService;

        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;

        private readonly IRepository<HoSo, int> _hoSoRepository;

        private readonly IRepository<Announcement, Guid> _announcementRepository;

        private readonly IRepository<AnnouncementUser> _announcementUserRepository;

        private readonly UserManager _userManager;

        private readonly string _connectionString;

        private readonly IEmailSender _emailSender;

        public QuyTrinhCongTacsAppService(IRepository<QuyTrinhCongTac> quyTrinhCongTacRepository, IRepository<TruongGiaoDich> truongGiaoDichRepository, IRepository<OrganizationUnit, long> organizationUnitRepository, IQuyTrinhCongTacsExcelExporter quyTrinhCongTacsExcelExporter, IRepository<HoSo, int> hoSoRepository, UserManager userManager, IWebHostEnvironment env, IRepository<Announcement, Guid> announcementRepository, IRepository<AnnouncementUser> announcementUserRepository, IEmailSender emailSender, IViewRenderAppService viewRenderService)
        {
            _quyTrinhCongTacRepository = quyTrinhCongTacRepository;
            _quyTrinhCongTacsExcelExporter = quyTrinhCongTacsExcelExporter;
            _hoSoRepository = hoSoRepository;
            _userManager = userManager;
            _announcementRepository = announcementRepository;
            _announcementUserRepository = announcementUserRepository;
            _emailSender = emailSender;
            _viewRenderService = viewRenderService;
            _organizationUnitRepository = organizationUnitRepository;
            _truongGiaoDichRepository = truongGiaoDichRepository;
            _connectionString = env.GetAppConfiguration().GetConnectionString("Default");
        }

        #region Angular

        [AbpAuthorize(AppPermissions.Pages_QuyTrinhCongTacs_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _quyTrinhCongTacRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetQuyTrinhCongTacsToExcel(GetAllQuyTrinhCongTacsForExcelInput input)
        {
            var filteredQuyTrinhCongTacs = _quyTrinhCongTacRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TenCty.Contains(input.Filter) || e.ViTriCongViecCode.Contains(input.Filter) || e.QuanLyTrucTiep.Contains(input.Filter) || e.TrangThaiCode.Contains(input.Filter) || e.GhiChu.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.TenCtyFilter), e => e.TenCty == input.TenCtyFilter)
                .WhereIf(input.MinDateToFilter != null, e => e.DateTo >= input.MinDateToFilter)
                .WhereIf(input.MaxDateToFilter != null, e => e.DateTo <= input.MaxDateToFilter)
                .WhereIf(input.MinDateFromFilter != null, e => e.DateFrom >= input.MinDateFromFilter)
                .WhereIf(input.MaxDateFromFilter != null, e => e.DateFrom <= input.MaxDateFromFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.ViTriCongViecCodeFilter), e => e.ViTriCongViecCode == input.ViTriCongViecCodeFilter)
                .WhereIf(input.MinDonViCongTacIDFilter != null, e => e.DonViCongTacID >= input.MinDonViCongTacIDFilter)
                .WhereIf(input.MaxDonViCongTacIDFilter != null, e => e.DonViCongTacID <= input.MaxDonViCongTacIDFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.QuanLyTrucTiepFilter), e => e.QuanLyTrucTiep == input.QuanLyTrucTiepFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TrangThaiCodeFilter), e => e.TrangThaiCode == input.TrangThaiCodeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.GhiChuFilter), e => e.GhiChu == input.GhiChuFilter);

            var query = (from o in filteredQuyTrinhCongTacs
                         select new GetQuyTrinhCongTacForViewDto
                         {
                             QuyTrinhCongTac = new QuyTrinhCongTacDto
                             {
                                 TenCty = o.TenCty,
                                 DateTo = o.DateTo,
                                 DateFrom = o.DateFrom,
                                 ViTriCongViecCode = o.ViTriCongViecCode,
                                 DonViCongTacID = o.DonViCongTacID,
                                 QuanLyTrucTiep = o.QuanLyTrucTiep,
                                 TrangThaiCode = o.TrangThaiCode,
                                 GhiChu = o.GhiChu,
                                 Id = o.Id
                             }
                         });

            var quyTrinhCongTacListDtos = await query.ToListAsync();

            return _quyTrinhCongTacsExcelExporter.ExportToFile(quyTrinhCongTacListDtos);
        }

        public async Task<PagedResultDto<GetQuyTrinhCongTacForViewDto>> GetAll(string id)
        {
            var filteredQuyTrinhCongTacs = _quyTrinhCongTacRepository.GetAll()
                .Where(x => x.MaHoSo == int.Parse(id));
            //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.TenCty.Contains(input.Filter) || e.ViTriCongViecCode.Contains(input.Filter) || e.QuanLyTrucTiep.Contains(input.Filter) || e.TrangThaiCode.Contains(input.Filter) || e.GhiChu.Contains(input.Filter))
            //.WhereIf(!string.IsNullOrWhiteSpace(input.TenCtyFilter),  e => e.TenCty == input.TenCtyFilter)
            //.WhereIf(input.MinDateToFilter != null, e => e.DateTo >= input.MinDateToFilter)
            //.WhereIf(input.MaxDateToFilter != null, e => e.DateTo <= input.MaxDateToFilter)
            //.WhereIf(input.MinDateFromFilter != null, e => e.DateFrom >= input.MinDateFromFilter)
            //.WhereIf(input.MaxDateFromFilter != null, e => e.DateFrom <= input.MaxDateFromFilter)
            //.WhereIf(!string.IsNullOrWhiteSpace(input.ViTriCongViecCodeFilter),  e => e.ViTriCongViecCode == input.ViTriCongViecCodeFilter)
            //.WhereIf(input.MinDonViCongTacIDFilter != null, e => e.DonViCongTacID >= input.MinDonViCongTacIDFilter)
            //.WhereIf(input.MaxDonViCongTacIDFilter != null, e => e.DonViCongTacID <= input.MaxDonViCongTacIDFilter)
            //.WhereIf(!string.IsNullOrWhiteSpace(input.QuanLyTrucTiepFilter),  e => e.QuanLyTrucTiep == input.QuanLyTrucTiepFilter)
            //.WhereIf(!string.IsNullOrWhiteSpace(input.TrangThaiCodeFilter),  e => e.TrangThaiCode == input.TrangThaiCodeFilter)
            //.WhereIf(!string.IsNullOrWhiteSpace(input.GhiChuFilter),  e => e.GhiChu == input.GhiChuFilter);

            var pagedAndFilteredQuyTrinhCongTacs = filteredQuyTrinhCongTacs;
            //.OrderBy(input.Sorting ?? "id asc")
            //.PageBy(input);

            var units = _organizationUnitRepository.GetAll();
            var tgd = _truongGiaoDichRepository.GetAll();
            var quyTrinhCongTacs = from o in pagedAndFilteredQuyTrinhCongTacs

                                   join unit in units on o.DonViCongTacID.Value equals unit.Id into unitjoin
                                   from joinedtunit in unitjoin.DefaultIfEmpty()

                                   join unit in units on o.ViTriCongViecCode equals unit.Code into unitjoin1
                                   from joinedtunit1 in unitjoin1.DefaultIfEmpty()

                                   join htlv in tgd.Where(x => x.Code == "TTNS") on o.TrangThaiCode equals htlv.CDName into htlvJoin
                                   from joinedhtlv in htlvJoin.DefaultIfEmpty()

                                   select new GetQuyTrinhCongTacForViewDto
                                   {
                                       QuyTrinhCongTac = new QuyTrinhCongTacDto
                                       {
                                           TenCty = o.TenCty,
                                           DateTo = o.DateTo,
                                           DateFrom = o.DateFrom,
                                           ViTriCongViecCode = o.ViTriCongViecCode,
                                           DonViCongTacID = o.DonViCongTacID,
                                           QuanLyTrucTiep = o.QuanLyTrucTiep,
                                           TrangThaiCode = o.TrangThaiCode,
                                           GhiChu = o.GhiChu,
                                           MaHoSo = o.MaHoSo,
                                           Id = o.Id
                                       },

                                       ViTriCongViecValue = joinedtunit1 == null ? "" : joinedtunit1.DisplayName.ToString(),
                                       DonViCongTacValue = joinedtunit == null ? "" : joinedtunit.DisplayName.ToString(),
                                       HinhThucLamViecValue = joinedhtlv == null ? "" : joinedhtlv.Value.ToString(),
                                   };

            var totalCount = await filteredQuyTrinhCongTacs.CountAsync();

            return new PagedResultDto<GetQuyTrinhCongTacForViewDto>(
                totalCount,
                await quyTrinhCongTacs.ToListAsync()
            );
        }

        public async Task<List<QuyTrinhCongTacDto>> GetListQuaTrinhCongTac(string id)
        {
            var listQTCT = await _quyTrinhCongTacRepository.GetAll().Where(x => x.MaHoSo == int.Parse(id)).ToListAsync();

            var listQTCTDto = ObjectMapper.Map<List<QuyTrinhCongTacDto>>(listQTCT);
            return listQTCTDto;
        }

        //public List<QuyTrinhCongTacDto> GetAllQuyTrinhCongTacList(int id)
        //{
        //    return _quyTrinhCongTacRepository.GetAll().Where(t => t.IsDeleted == false).Select(t => new QuyTrinhCongTacDto { Id=t.Id , MaHoSo = t.MaHoSo  }).ToList();
        //}

        public async Task CreateOrEdit(CreateOrEditQuyTrinhCongTacDto input)
        {
            try
            {
                if (input.Id == null)
                {
                    await Create(input);
                }
                else
                {
                    await Update(input);
                }
            }
            catch (Exception e)
            {
                Logger.Warn(e.Message, e);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_QuyTrinhCongTacs_Create)]
        protected virtual async Task Create(CreateOrEditQuyTrinhCongTacDto input)
        {
            var quyTrinhCongTac = ObjectMapper.Map<QuyTrinhCongTac>(input);

            await _quyTrinhCongTacRepository.InsertAsync(quyTrinhCongTac);
        }

        //[AbpAuthorize(AppPermissions.Pages_QuyTrinhCongTacs_Edit)]
        protected virtual async Task Update(CreateOrEditQuyTrinhCongTacDto input)
        {
            var quyTrinhCongTac = await _quyTrinhCongTacRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, quyTrinhCongTac);
        }

        [AbpAuthorize(AppPermissions.Pages_QuyTrinhCongTacs_Edit)]
        public async Task<GetQuyTrinhCongTacForEditOutput> GetQuyTrinhCongTacForEdit(EntityDto input)
        {
            var quyTrinhCongTac = await _quyTrinhCongTacRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetQuyTrinhCongTacForEditOutput { QuyTrinhCongTac = ObjectMapper.Map<CreateOrEditQuyTrinhCongTacDto>(quyTrinhCongTac) };

            return output;
        }

        #endregion Angular

        #region Method Supports

        private double MathTimeViolatingRuleDurationFirst(DateTime processDate, string timeCheck, TruongGiaoDich workTimeMonday, TruongGiaoDich workTimeDaily, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (IsWorkStartMorning(timeCheckList, middleStart))
            {
                switch (processDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (workTimeMonday != null && timeCheckList.Any())
                        {
                            double minutesFirst = (timeCheckList.First() - workTimeMonday.Value.ToTimeSpan()).TotalMinutes;
                            result = minutesFirst > 0 ? minutesFirst : 0;
                        }
                        break;

                    default:
                        if (workTimeDaily != null && timeCheckList.Any())
                        {
                            double minutesFirst = (timeCheckList.First() - workTimeDaily.Value.ToTimeSpan()).TotalMinutes;
                            result = minutesFirst > 0 ? minutesFirst : 0;
                        }
                        break;
                }
            }
            if (IsWorkStartAfternoon(timeCheckList, middleStart, middleEnd))
            {
                if (middleEnd != null && timeCheckList.Any())
                {
                    double minutesFirst = (timeCheckList.First() - middleEnd.Value.ToTimeSpan()).TotalMinutes;
                    result = minutesFirst > 0 ? minutesFirst : 0;
                }
            }
            return result;
        }

        private double MathTimeViolatingRuleDurationLast(string timeCheck, TruongGiaoDich middleEnd, TruongGiaoDich workTimeEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (workTimeEnd != null && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkAfternoon(timeCheckList, middleEnd))
            {
                double minutesLast = (workTimeEnd.Value.ToTimeSpan() - timeCheckList.Last()).TotalMinutes;
                result = minutesLast > 0 ? minutesLast : 0;
            }
            return result;
        }

        private double MathTimeOTDurationDaily(string timeCheck, TruongGiaoDich workTimeEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (workTimeEnd != null && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5)
            {
                double minutesLast = (timeCheckList.Last() - workTimeEnd.Value.ToTimeSpan()).TotalMinutes;
                result += minutesLast > 0 ? minutesLast : 0;
            }
            return result;
        }

        private double MathTimeWorkMorningDuration(string timeCheck, DateTime processDate,
            TruongGiaoDich workTimeMonday, TruongGiaoDich workTimeDaily,
            TruongGiaoDich workTimeApprove, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (middleStart != null && middleEnd != null && workTimeApprove != null
                && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkStartMorning(timeCheckList, middleStart))
            {
                if (processDate.DayOfWeek == DayOfWeek.Monday)
                {
                    var workStart = timeCheckList.First() >= workTimeMonday.Value.ToTimeSpan() ? timeCheckList.First() : workTimeMonday.Value.ToTimeSpan();
                    var workEnd = timeCheckList.Last() >= middleStart.Value.ToTimeSpan() ? middleStart.Value.ToTimeSpan() : timeCheckList.Last();
                    var totalMorning = (workEnd - workStart).TotalMinutes;
                    result = totalMorning > 0 && totalMorning > Convert.ToDouble(workTimeApprove.Value) ? totalMorning : 0;
                }
                else
                {
                    var workStart = timeCheckList.First() >= workTimeDaily.Value.ToTimeSpan() ? timeCheckList.First() : workTimeDaily.Value.ToTimeSpan();
                    var workEnd = timeCheckList.Last() >= middleStart.Value.ToTimeSpan() ? middleStart.Value.ToTimeSpan() : timeCheckList.Last();
                    var totalMorning = (workEnd - workStart).TotalMinutes;
                    result = totalMorning > 0 && totalMorning > Convert.ToDouble(workTimeApprove.Value) ? totalMorning : 0;
                }
            }
            return result;
        }

        private double MathTimeWorkAfternoonDuration(string timeCheck, TruongGiaoDich workTimeEnd,
            TruongGiaoDich workTimeApprove, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (middleStart != null && middleEnd != null && workTimeApprove != null && workTimeEnd != null
                && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkAfternoon(timeCheckList, middleEnd))
            {
                var workStart = timeCheckList.First() >= middleEnd.Value.ToTimeSpan() ? timeCheckList.First() : middleEnd.Value.ToTimeSpan();
                var workEnd = timeCheckList.Last() <= workTimeEnd.Value.ToTimeSpan() ? timeCheckList.Last() : workTimeEnd.Value.ToTimeSpan();
                var totalAfternoon = (workEnd - workStart).TotalMinutes;
                result = totalAfternoon > 0 && totalAfternoon > Convert.ToDouble(workTimeApprove.Value) ? totalAfternoon : 0;
            }

            return result;
        }

        private bool IsWorkStartMorning(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleStart)
        {
            return timeCheckList.First() < middleStart.Value.ToTimeSpan();
        }

        private bool IsWorkStartAfternoon(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            return timeCheckList.First() > middleStart.Value.ToTimeSpan() && timeCheckList.First() < middleEnd.Value.ToTimeSpan();
        }

        private bool IsWorkAfternoon(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleEnd)
        {
            return timeCheckList.Last() > middleEnd.Value.ToTimeSpan();
        }

        #endregion Method Supports
    }
}