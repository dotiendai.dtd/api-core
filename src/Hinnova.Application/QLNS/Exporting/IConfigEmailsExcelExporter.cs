﻿using Hinnova.Dto;
using Hinnova.QLNS.Dtos;
using System.Collections.Generic;

namespace Hinnova.QLNS.Exporting
{
    public interface IConfigEmailsExcelExporter
    {
        FileDto ExportToFile(List<GetConfigEmailForViewDto> configEmails);
    }
}