﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace Hinnova.Utils.Storage
{
    public class StorageService : HinnovaServiceBase, IStorageService
    {
        private readonly string _controllerFolder;

        public StorageService(IWebHostEnvironment webHostEnvironment)
        {
            _controllerFolder = Path.Combine(webHostEnvironment.WebRootPath, "attachments");
        }

        public string GetFileUrl(string fileName)
        {
            return $"/attachments/{fileName}";
        }

        public async Task SaveFileAsync(Stream mediaBinaryStream, string fileName)
        {
            if (!Directory.Exists(_controllerFolder)) 
                Directory.CreateDirectory(_controllerFolder);
            var filePath = Path.Combine(_controllerFolder, fileName);
            using (var output = new FileStream(filePath, FileMode.Create))
            {
                await mediaBinaryStream.CopyToAsync(output);
            }
        }
    }
}
