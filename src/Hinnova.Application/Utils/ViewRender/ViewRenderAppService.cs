﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;

namespace Hinnova.Utils.ViewRender
{
    public class ViewRenderAppService : HinnovaServiceBase, IViewRenderAppService
    {
        private readonly IRazorViewEngine _razorViewEngine;

        private readonly IServiceProvider _serviceProvider;

        private readonly ITempDataProvider _tempDataProvider;

        public ViewRenderAppService(IRazorViewEngine razorViewEngine,
            ITempDataProvider tempDataProvider,
            IServiceProvider serviceProvider)
        {
            _razorViewEngine = razorViewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<string> RenderViewAsync(string name, object objModel)
        {
            using (StringWriter sw = new StringWriter())
            {
                ActionContext actionContext = new ActionContext(new DefaultHttpContext { RequestServices = _serviceProvider }, new RouteData(), new ActionDescriptor());
                ViewEngineResult result = _razorViewEngine.FindView(actionContext, name, false);
                if (result.View == null)
                    throw new ArgumentNullException($"{name} does not match any available view");
                await result.View.RenderAsync(new ViewContext(actionContext, result.View, new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary()) { Model = objModel }, new TempDataDictionary(actionContext.HttpContext, _tempDataProvider), sw, new HtmlHelperOptions()));
                return sw.ToString();
            }
        }
    }
}
