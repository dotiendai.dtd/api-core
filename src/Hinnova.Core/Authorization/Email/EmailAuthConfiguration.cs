﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hinnova.Web.Authentication.Email
{
    public class EmailAuthConfiguration
    {
        public string Server { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public int Port { get; set; }
        public bool EnableSsl { get; set; }
    }
}
