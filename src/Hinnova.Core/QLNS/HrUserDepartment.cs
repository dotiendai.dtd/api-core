﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS
{
    [Table("HR_UserDepartment")]
    public class HrUserDepartment : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get => UserId + "@" + DepartmentId;
            set { }
        }
        public long UserId { get; set; }

        public string DepartmentId { get; set; }

    }
}
