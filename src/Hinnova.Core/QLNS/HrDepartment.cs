﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS
{
    [Table("HR_Department")]
    public class HrDepartment : Entity<string>
    {
        public string CompanyId { get; set; }

        public string DepartmentName { get; set; }

        public bool Active { get; set; }
    }
}
