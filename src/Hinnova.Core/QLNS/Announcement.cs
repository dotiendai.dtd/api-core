﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Authorization.Users;
using Abp.Domain.Entities;
using Hinnova.Authorization.Users;

namespace Hinnova.QLNS
{
    [Table("Announcements")]
    public class Announcement : Entity<Guid>
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Image { get; set; }
        [Required]
        public long UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public bool Status { get; set; }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public List<AnnouncementUser> AnnouncementUsers { get; set; }
        public User User { get; set; }
    }
}
