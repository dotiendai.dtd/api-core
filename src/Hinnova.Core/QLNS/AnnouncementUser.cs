﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS
{
    [Table("AnnouncementUsers")]
    public class AnnouncementUser : Entity
    {
        public long? UserId { get; set; }
        public bool HasRead { get; set; }
        [Required]
        public Guid AnnouncementId { get; set; }

        public Announcement Announcement { get; set; }
    }
}
