﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;

namespace Hinnova.QLNS
{
    [Table("HR_Worktime")]
    public class HrWorkTime : FullAuditedEntity
    {
        public string DocumentType { get; set; }

        public DateTime TimeFrom { get; set; }

        public DateTime TimeTo { get; set; }

        public double? TotalMinutes { get; set; }

        public string Reasons { get; set; }

        public string Notes { get; set; }

        public string Attachment { get; set; }

        public short Status { get; set; }

        public long? ApproverId { get; set; }

        public long? NextApproverId { get; set; }

        public DateTime? ApproveTime { get; set; }

        public long? TruongNhomId { get; set; }

        public long? TruongPhongId { get; set; }

        public long? GiamDocKhoiId { get; set; }

        public long TcnsId { get; set; }

        public long GiamDocDieuHanhId { get; set; }

    }
}
