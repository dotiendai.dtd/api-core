﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS
{
    [Table("HR_Company")]
    public class HrCompany : Entity<string>
    {
        public string CompanyName { get; set; }

        public bool Active { get; set; }
    }
}
