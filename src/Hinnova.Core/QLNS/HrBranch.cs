﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS
{
    [Table("HR_Branch")]
    public class HrBranch : Entity<int>
    {
        public int FatherId { get; set; }

        public string BranchCode { get; set; }

        public string BranchName { get; set; }

        public string BranchType { get; set; }
    }
}
