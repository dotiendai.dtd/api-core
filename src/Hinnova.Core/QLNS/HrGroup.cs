﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS
{
    [Table("HR_Group")]
    public class HrGroup : Entity<string>
    {
        public string DepartmentId { get; set; }

        public string CompanyId { get; set; }

        public string GroupName { get; set; }

        public bool Active { get; set; }
    }
}
