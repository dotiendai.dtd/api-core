﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS
{
    [Table("HR_UserGroup")]
    public class HrUserGroup : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get => UserId + "@" + GroupId;
            set { }
        }
        public long UserId { get; set; }


        public string GroupId { get; set; }
    }
}
