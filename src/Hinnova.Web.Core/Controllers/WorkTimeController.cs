﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using Abp.Organizations;
using Abp.UI;
using Dapper;
using Hinnova.Authorization.Users;
using Hinnova.Configuration;
using Hinnova.QLNS;
using Hinnova.QLNS.Dtos;
using Hinnova.Utils;
using Hinnova.Web.Authentication.Connection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Hinnova.Web.Helpers;

namespace Hinnova.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    public class WorkTimeController : HinnovaControllerBase
    {

        private readonly IRepository<HoSo, int> _hoSoRepository;

        private readonly IViewRenderAppService _viewRenderService;

        private readonly IRepository<Announcement, Guid> _announceRepository;

        private readonly IRepository<AnnouncementUser> _announceUserRepository;

        private readonly UserManager _userManager;

        private readonly IEmailSender _emailSender;

        private readonly IRepository<HrWorkTime> _hrWorkTimeRepository;

        private readonly IRepository<TruongGiaoDich> _truongGiaoDichRepository;

        private readonly IStorageService _storageService;

        public WorkTimeController( IRepository<HoSo, int> hoSoRepository, IViewRenderAppService viewRenderService, IRepository<Announcement, Guid> announceRepository, IRepository<AnnouncementUser> announceUserRepository, UserManager userManager, IEmailSender emailSender, IRepository<HrWorkTime> hrWorkTimeRepository, IRepository<TruongGiaoDich> truongGiaoDichRepository, IStorageService storageService)
        {
            _hoSoRepository = hoSoRepository;
            _viewRenderService = viewRenderService;
            _announceRepository = announceRepository;
            _announceUserRepository = announceUserRepository;
            _userManager = userManager;
            _emailSender = emailSender;
            _hrWorkTimeRepository = hrWorkTimeRepository;
            _truongGiaoDichRepository = truongGiaoDichRepository;
            _storageService = storageService;
        }

        #region Mobile

        [HttpPost]
        public async Task<IActionResult> CreateOrEditForMobile([FromForm] HrWorkTimeDtoRequest _)
        {
            //SendUserDto
            try
            {
                if (DateTime.Compare(DateTime.Parse(_.TimeFrom).Date, DateTime.Parse(_.TimeTo).Date) > 0)
                    throw new UserFriendlyException(L(name: "Ngày kết thúc lớn hơn ngày bắt đầu"));
                if (_.Id == 0)
                {
                    return Ok(await CreateForMobile(_));
                }
                return Ok(await EditForMobile(_));
            }
            catch (Exception e)
            {
                Logger.Warn(e.Message, e);
                return BadRequest(new ApiBadRequestResponse(e.Message));
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        protected async Task<SendUserDto> CreateForMobile(HrWorkTimeDtoRequest _)
        {
            _.NextApproverId = _.TruongNhomId ?? _.TruongPhongId ?? _.GiamDocKhoiId ?? _.TcnsId;
            string nameTitle;
            switch (_.DocumentType)
            {
                case "NP":
                    {
                        nameTitle = "Nghỉ Phép";
                        break;
                    }
                case "CT":
                    {
                        nameTitle = "Công Tác";
                        break;
                    }
                case "TC":
                    {
                        nameTitle = "Tăng Ca";
                        break;
                    }
                default:
                    {
                        nameTitle = "Quên Chấm Công";
                        break;
                    }

            }
            _.Status = 0;
            var from = DateTime.Parse(_.TimeFrom);
            var to = DateTime.Parse(_.TimeTo);
            var workTimeMonday = await _truongGiaoDichRepository.FirstOrDefaultAsync(x => x.Code.Equals(TruongGiaoDichConsts.WORKTIME) && x.CDName.Equals(DataChamCongConsts.WORKTIME_START_MONDAY));
            var workTimeDaily = await _truongGiaoDichRepository.FirstOrDefaultAsync(x => x.Code.Equals(TruongGiaoDichConsts.WORKTIME) && x.CDName.Equals(DataChamCongConsts.WORKTIME_START_DAILY));
            var workTimeEnd = await _truongGiaoDichRepository.FirstOrDefaultAsync(x => x.Code.Equals(TruongGiaoDichConsts.WORKTIME) && x.CDName.Equals(DataChamCongConsts.WORKTIME_END));
            var workTimeApprove = await _truongGiaoDichRepository.FirstOrDefaultAsync(x => x.Code.Equals(TruongGiaoDichConsts.WORKTIME) && x.CDName.Equals(DataChamCongConsts.WORK_TIME_APPROVE));
            var middleStart = await _truongGiaoDichRepository.FirstOrDefaultAsync(x => x.Code.Equals(TruongGiaoDichConsts.WORKTIME) && x.CDName.Equals(DataChamCongConsts.MIDDLE_START));
            var middleEnd = await _truongGiaoDichRepository.FirstOrDefaultAsync(x => x.Code.Equals(TruongGiaoDichConsts.WORKTIME) && x.CDName.Equals(DataChamCongConsts.MIDDLE_END));
            var timeDuration = (to.Date - from.Date).TotalDays;
            var workTime = new HrWorkTime()
            {
                Status = _.Status.Value,
                DocumentType = _.DocumentType,
                ApproverId = null,
                NextApproverId = _.NextApproverId.Value,
                GiamDocDieuHanhId = _.GiamDocDieuHanhId,
                GiamDocKhoiId = _.GiamDocKhoiId,
                ApproveTime = null,
                Notes = null,
                Reasons = _.Reasons,
                TimeFrom = from,
                TimeTo = to,
                TcnsId = _.TcnsId,
                TruongNhomId = _.TruongNhomId,
                TruongPhongId = _.TruongPhongId
            };
            if (_.Attachment != null)
            {
                var file = $"{Guid.NewGuid()}{Path.GetExtension(ContentDispositionHeaderValue.Parse(_.Attachment.ContentDisposition).FileName.Trim('"'))}";
                await _storageService.SaveFileAsync(_.Attachment.OpenReadStream(), file);
                workTime.Attachment = _storageService.GetFileUrl(file);
            }
            if (timeDuration == 0)
            {
                var checkTime = $"{from:HH:mm:ss}~{to:HH:mm:ss}";
                if (_.DocumentType.Equals("TC"))
                {
                    workTime.TotalMinutes = MathTimeWorkMorningDuration(checkTime, from.Date, workTimeMonday, workTimeDaily, workTimeApprove, middleStart, middleEnd) + MathTimeWorkAfternoonDuration(checkTime, workTimeEnd, workTimeApprove, middleStart, middleEnd) + MathTimeOTDurationDaily(checkTime, workTimeEnd);
                }
                else
                {
                    workTime.TotalMinutes = MathTimeWorkMorningDuration(checkTime, from.Date, workTimeMonday, workTimeDaily, workTimeApprove, middleStart, middleEnd) + MathTimeWorkAfternoonDuration(checkTime, workTimeEnd, workTimeApprove, middleStart, middleEnd);
                }
            }
            else
            {
                var timeRegister = from.Date;
                double totalTimeWorkMorningDuration = 0;
                double totalTimeWorkAfternoonDuration = 0;
                var checkTime = "07:00:00~18:00:00";
                for (var i = 0; i <= timeDuration; i++)
                {
                    totalTimeWorkMorningDuration += MathTimeWorkMorningDuration(checkTime, timeRegister,
                        workTimeMonday, workTimeDaily, workTimeApprove, middleStart, middleEnd);
                    totalTimeWorkAfternoonDuration += MathTimeWorkAfternoonDuration(checkTime, workTimeEnd,
                        workTimeApprove, middleStart, middleEnd);
                    timeRegister = timeRegister.AddDays(1);
                }
                workTime.TotalMinutes = totalTimeWorkMorningDuration + totalTimeWorkAfternoonDuration;
            }
            var workTimeId = await _hrWorkTimeRepository.InsertAndGetIdAsync(workTime);
            var announcementId = Guid.NewGuid();
            var now = DateTime.Now;
            var announcement = new Announcement
            {
                Title = $"{_.HoVaTen} đã gửi đơn đăng ký {nameTitle}",
                DateCreated = now,
                Content = $"Xin Đi Công Tác Từ Ngày {_.TimeFrom} Đến Ngày {_.TimeTo} Với Lý Do {_.Reasons}",
                Id = announcementId,
                UserId = _.CreatorUserId,
                Image = _.Image ?? "/uploads/defaultAvatar.png",
                Status = true,
                DateModified = now,
                EntityType = _.DocumentType,
                EntityId = workTimeId.ToString()
            };
            await _announceRepository.InsertAndGetIdAsync(announcement);
            var announcementUser = new AnnouncementUser
            {
                AnnouncementId = announcementId,
                HasRead = false,
                UserId = _.NextApproverId
            };
            var announceUIdDto = await _announceUserRepository.InsertOrUpdateAndGetIdAsync(announcementUser);
            await _emailSender.SendAsync("long0072017@gmail.com", announcement.Title, await _viewRenderService.RenderViewAsync("Utils/ContentEmail", new HrWorkTimeDto
            {
                HoVaTen = _.HoVaTen,
                TenCty = _.TenCty,
                TimeFrom = _.TimeFrom,
                TimeTo = _.TimeTo,
                Reasons = _.Reasons,
                Attachment = _storageService.GetFileUrl(workTime.Attachment),
                DocumentType = nameTitle,
            }));
            var sendUser = new SendUserDto();
            sendUser.AnnouncementDtos.Add(new AnnouncementDto
            {
                Title = $"{_.HoVaTen} đã gửi đơn đăng ký {nameTitle}",
                DateCreated = now,
                Content = $"Xin Đi Công Tác Từ Ngày {_.TimeFrom} Đến Ngày {_.TimeTo} Với Lý Do {_.Reasons}",
                Id = announcementId,
                UserId = _.CreatorUserId,
                Image = _.Image ?? "/uploads/defaultAvatar.png",
                Status = true,
                DateModified = now,
                EntityType = _.DocumentType,
                EntityId = workTimeId.ToString()
            });
            sendUser.AnnouncementUserDtos.Add(new AnnouncementUserDto
            {
                AnnouncementId = announcementId,
                HasRead = false,
                UserId = _.NextApproverId,
                Id = announceUIdDto
            });
            return sendUser;
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        protected async Task<SendUserDto> EditForMobile(HrWorkTimeDtoRequest input)
        {
            var detail = await _hrWorkTimeRepository.FirstOrDefaultAsync(_ => _.Id == input.Id);
            if (detail == null)
            {
                Logger.Error("Không Có Đơn Này!");
                throw new Exception("Không Có Đơn Này!");
            }
            if (!string.IsNullOrEmpty(detail.Notes))
            {
                Logger.Error("Đơn Này Không Được Duyệt!");
                throw new Exception("Đơn Này Không Được Duyệt!");
            }
            var check = await _userManager.FindByIdAsync(input.NextApproverId.ToString());
            var create = await _userManager.FindByIdAsync(detail.CreatorUserId.ToString());
            var info = await _hoSoRepository.SingleAsync(_ => _.MaNhanVien == create.EmployeeCode);
            var send = new SendUserDto();
            var now = DateTime.Now;
            var id = Guid.NewGuid();
            string title;
            switch (detail.DocumentType)
            {
                case "NP":
                    {
                        title = "Nghỉ Phép";
                        break;
                    }
                case "CT":
                    {
                        title = "Công Tác";
                        break;
                    }
                case "TC":
                    {
                        title = "Tăng Ca";
                        break;
                    }
                default:
                    {
                        title = "Quên Chấm Công";
                        break;
                    }

            }
            var checkAnnounce = (await _announceRepository.GetAllListAsync(
                _ => _.UserId == create.Id)).Join((await _announceUserRepository.GetAllListAsync()), _ => _.Id,
                _ => _.AnnouncementId,
                (_, __) => new { _, __ }).ToList().Where(_ =>
                _._.UserId == create.Id &&
                _.__.UserId == check.Id && _._.EntityId.Equals(detail.Id.ToString())).ToList().Select(
                _ => new AnnouncementDto
                {
                    Status = _._.Status,
                    UserId = _._.UserId,
                    DateCreated = _._.DateCreated,
                    Content = _._.Content,
                    Title = _._.Title,
                    Image = _._.Image,
                    DateModified = _._.DateModified,
                    Id = _._.Id,
                    EntityId = _._.EntityId,
                    EntityType = _._.EntityType
                }).FirstOrDefault();
            if (checkAnnounce == null)
            {
                Logger.Error("Không tìm thấy thông báo User tạo đơn");
                throw new Exception("Không tìm thấy thông báo User tạo đơn");
            }
            detail.ApproveTime = DateTime.Parse(now.ToString("yyyy-MM-dd HH:mm"));
            var sendAnnounce = new Announcement
            {
                Title = input.Notes != null ? $"{input.HoVaTen} Đã Từ Chối Duyệt Đơn {title} Của Bạn {info.HoVaTen}" : $"{input.HoVaTen} Đã Duyệt Đơn {title} Của Bạn {info.HoVaTen}",
                DateCreated = now,
                Content = input.Notes != null ? $"Lý Do Từ Chối Duyệt: {input.Notes}" : $"Ngày Duyệt Đơn Của Bạn: {now:dd/MM/yyyy}",
                Id = id,
                UserId = check.Id,
                Image = input.Image ?? "/uploads/defaultAvatar.png",
                Status = false,
                DateModified = now,
                EntityType = detail.DocumentType,
                EntityId = detail.Id.ToString()
            };
            await _announceRepository.InsertAndGetIdAsync(sendAnnounce);
            if (string.IsNullOrEmpty(input.Notes) && !detail.NextApproverId.Equals(detail.GiamDocDieuHanhId) || !detail.NextApproverId.Equals(detail.GiamDocDieuHanhId))
            {
                detail.ApproverId = detail.NextApproverId;
                if (detail.NextApproverId.Equals(detail.TruongNhomId) && !string.IsNullOrEmpty(detail.TruongPhongId.ToString()))
                {
                    detail.NextApproverId = detail.TruongPhongId;
                }
                else if (detail.NextApproverId.Equals(detail.TruongPhongId) && !string.IsNullOrEmpty(detail.GiamDocKhoiId.ToString()))
                {
                    detail.NextApproverId = detail.GiamDocKhoiId;
                }
                else if (detail.NextApproverId.Equals(detail.GiamDocKhoiId) && !string.IsNullOrEmpty(detail.TcnsId.ToString()))
                {
                    detail.NextApproverId = detail.TcnsId;
                }
                else if (detail.NextApproverId.Equals(detail.TcnsId) && !string.IsNullOrEmpty(detail.GiamDocDieuHanhId.ToString()))
                {
                    detail.NextApproverId = detail.GiamDocDieuHanhId;
                }
                var announceUId = await _announceUserRepository.InsertAndGetIdAsync(new AnnouncementUser
                {
                    AnnouncementId = checkAnnounce.Id,
                    HasRead = false,
                    UserId = detail.NextApproverId
                });
                send.AnnouncementDtos.Add(checkAnnounce);
                send.AnnouncementUserDtos.Add(new AnnouncementUserDto
                {
                    AnnouncementId = checkAnnounce.Id,
                    HasRead = false,
                    UserId = detail.NextApproverId,
                    Id = announceUId
                });
                await _emailSender.SendAsync("long0072017@gmail.com", checkAnnounce.Title, await _viewRenderService.RenderViewAsync("Utils/ContentEmail", new HrWorkTimeDto
                {
                    HoVaTen = info.HoVaTen,
                    TenCty = info.TenCty,
                    TimeFrom = detail.TimeFrom.ToString(CultureInfo.CurrentCulture),
                    TimeTo = detail.TimeTo.ToString(CultureInfo.CurrentCulture),
                    Reasons = detail.Reasons,
                    Attachment = detail.Attachment != null ? _storageService.GetFileUrl(detail.Attachment) : null,
                    DocumentType = title,
                }));
            }
            else
            {
                var save = detail.NextApproverId;
                if (detail.NextApproverId.Equals(detail.GiamDocDieuHanhId) && string.IsNullOrEmpty(input.Notes))
                {
                    detail.Status = 1;
                }
                else
                {
                    detail.Notes = input.Notes;
                    detail.Status = 2;
                }
                if (save.Equals(detail.GiamDocDieuHanhId))
                {
                    save = detail.TcnsId;
                    var announceUIdDto = await _announceUserRepository.InsertAndGetIdAsync(new AnnouncementUser
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save
                    });
                    send.AnnouncementUserDtos.Add(new AnnouncementUserDto
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save,
                        Id = announceUIdDto
                    });
                    await _emailSender.SendAsync("long0072017@gmail.com", sendAnnounce.Title, sendAnnounce.Content);
                }
                else if (save.Equals(detail.TcnsId) && detail.GiamDocKhoiId != null)
                {
                    save = detail.GiamDocKhoiId;
                    var announceUIdDto = await _announceUserRepository.InsertAndGetIdAsync(new AnnouncementUser
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save
                    });
                    send.AnnouncementUserDtos.Add(new AnnouncementUserDto
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save,
                        Id = announceUIdDto
                    });
                    await _emailSender.SendAsync("long0072017@gmail.com", sendAnnounce.Title, sendAnnounce.Content);
                }
                else if (save.Equals(detail.GiamDocKhoiId) && detail.TruongPhongId != null)
                {
                    save = detail.TruongPhongId;
                    var announceUIdDto = await _announceUserRepository.InsertAndGetIdAsync(new AnnouncementUser
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save
                    });
                    send.AnnouncementUserDtos.Add(new AnnouncementUserDto
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save,
                        Id = announceUIdDto
                    });
                    await _emailSender.SendAsync("long0072017@gmail.com", sendAnnounce.Title, sendAnnounce.Content);
                }
                else if (save.Equals(detail.TruongPhongId) && detail.TruongNhomId != null)
                {
                    save = detail.TruongNhomId;
                    var announceUIdDto = await _announceUserRepository.InsertAndGetIdAsync(new AnnouncementUser
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save
                    });
                    send.AnnouncementUserDtos.Add(new AnnouncementUserDto
                    {
                        AnnouncementId = sendAnnounce.Id,
                        HasRead = false,
                        UserId = save,
                        Id = announceUIdDto
                    });
                    await _emailSender.SendAsync("long0072017@gmail.com", sendAnnounce.Title, sendAnnounce.Content);
                }
            }
            send.AnnouncementDtos.Add(new AnnouncementDto
            {
                Title = sendAnnounce.Title,
                DateCreated = sendAnnounce.DateCreated,
                Content = sendAnnounce.Content,
                Id = sendAnnounce.Id,
                UserId = sendAnnounce.UserId,
                Image = sendAnnounce.Image,
                Status = sendAnnounce.Status,
                EntityId = sendAnnounce.EntityId,
                DateModified = sendAnnounce.DateModified,
                EntityType = sendAnnounce.EntityType
            });
            var announceUIdDto1 = await _announceUserRepository.InsertAndGetIdAsync(new AnnouncementUser
            {
                AnnouncementId = sendAnnounce.Id,
                HasRead = false,
                UserId = create.Id
            });
            send.AnnouncementUserDtos.Add(new AnnouncementUserDto
            {
                AnnouncementId = sendAnnounce.Id,
                HasRead = false,
                UserId = create.Id,
                Id = announceUIdDto1
            });
            await _emailSender.SendAsync("long0072017@gmail.com", sendAnnounce.Title, sendAnnounce.Content);
            return send;
        }
        #endregion

        #region Method Supports

        private double MathTimeViolatingRuleDurationFirst(DateTime processDate, string timeCheck, TruongGiaoDich workTimeMonday, TruongGiaoDich workTimeDaily, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (IsWorkStartMorning(timeCheckList, middleStart))
            {
                switch (processDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (workTimeMonday != null && timeCheckList.Any())
                        {
                            double minutesFirst = (timeCheckList.First() - workTimeMonday.Value.ToTimeSpan()).TotalMinutes;
                            result = minutesFirst > 0 ? minutesFirst : 0;
                        }
                        break;

                    default:
                        if (workTimeDaily != null && timeCheckList.Any())
                        {
                            double minutesFirst = (timeCheckList.First() - workTimeDaily.Value.ToTimeSpan()).TotalMinutes;
                            result = minutesFirst > 0 ? minutesFirst : 0;
                        }
                        break;
                }
            }
            if (IsWorkStartAfternoon(timeCheckList, middleStart, middleEnd))
            {
                if (middleEnd != null && timeCheckList.Any())
                {
                    double minutesFirst = (timeCheckList.First() - middleEnd.Value.ToTimeSpan()).TotalMinutes;
                    result = minutesFirst > 0 ? minutesFirst : 0;
                }
            }
            return result;
        }

        private double MathTimeViolatingRuleDurationLast(string timeCheck, TruongGiaoDich middleEnd, TruongGiaoDich workTimeEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (workTimeEnd != null && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkAfternoon(timeCheckList, middleEnd))
            {
                double minutesLast = (workTimeEnd.Value.ToTimeSpan() - timeCheckList.Last()).TotalMinutes;
                result = minutesLast > 0 ? minutesLast : 0;
            }
            return result;
        }

        private double MathTimeOTDurationDaily(string timeCheck, TruongGiaoDich workTimeEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (workTimeEnd != null && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5)
            {
                double minutesLast = (timeCheckList.Last() - workTimeEnd.Value.ToTimeSpan()).TotalMinutes;
                result += minutesLast > 0 ? minutesLast : 0;
            }
            return result;
        }

        private double MathTimeWorkMorningDuration(string timeCheck, DateTime processDate,
            TruongGiaoDich workTimeMonday, TruongGiaoDich workTimeDaily,
            TruongGiaoDich workTimeApprove, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (middleStart != null && middleEnd != null && workTimeApprove != null
                && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkStartMorning(timeCheckList, middleStart))
            {
                if (processDate.DayOfWeek == DayOfWeek.Monday)
                {
                    var workStart = timeCheckList.First() >= workTimeMonday.Value.ToTimeSpan() ? timeCheckList.First() : workTimeMonday.Value.ToTimeSpan();
                    var workEnd = timeCheckList.Last() >= middleStart.Value.ToTimeSpan() ? middleStart.Value.ToTimeSpan() : timeCheckList.Last();
                    var totalMorning = (workEnd - workStart).TotalMinutes;
                    result = totalMorning > 0 && totalMorning > Convert.ToDouble(workTimeApprove.Value) ? totalMorning : 0;
                }
                else
                {
                    var workStart = timeCheckList.First() >= workTimeDaily.Value.ToTimeSpan() ? timeCheckList.First() : workTimeDaily.Value.ToTimeSpan();
                    var workEnd = timeCheckList.Last() >= middleStart.Value.ToTimeSpan() ? middleStart.Value.ToTimeSpan() : timeCheckList.Last();
                    var totalMorning = (workEnd - workStart).TotalMinutes;
                    result = totalMorning > 0 && totalMorning > Convert.ToDouble(workTimeApprove.Value) ? totalMorning : 0;
                }
            }
            return result;
        }

        private double MathTimeWorkAfternoonDuration(string timeCheck, TruongGiaoDich workTimeEnd,
            TruongGiaoDich workTimeApprove, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            double result = 0;
            var timeCheckList = timeCheck.Split('~')?.Select(x => x.ToTimeSpan());

            if (middleStart != null && middleEnd != null && workTimeApprove != null && workTimeEnd != null
                && timeCheckList != null && timeCheckList.Count() > 1
                && (timeCheckList.Last() - timeCheckList.First()).TotalMinutes > 5
                && IsWorkAfternoon(timeCheckList, middleEnd))
            {
                var workStart = timeCheckList.First() >= middleEnd.Value.ToTimeSpan() ? timeCheckList.First() : middleEnd.Value.ToTimeSpan();
                var workEnd = timeCheckList.Last() <= workTimeEnd.Value.ToTimeSpan() ? timeCheckList.Last() : workTimeEnd.Value.ToTimeSpan();
                var totalAfternoon = (workEnd - workStart).TotalMinutes;
                result = totalAfternoon > 0 && totalAfternoon > Convert.ToDouble(workTimeApprove.Value) ? totalAfternoon : 0;
            }

            return result;
        }

        private bool IsWorkStartMorning(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleStart)
        {
            return timeCheckList.First() < middleStart.Value.ToTimeSpan();
        }

        private bool IsWorkStartAfternoon(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleStart, TruongGiaoDich middleEnd)
        {
            return timeCheckList.First() > middleStart.Value.ToTimeSpan() && timeCheckList.First() < middleEnd.Value.ToTimeSpan();
        }

        private bool IsWorkAfternoon(IEnumerable<TimeSpan> timeCheckList, TruongGiaoDich middleEnd)
        {
            return timeCheckList.Last() > middleEnd.Value.ToTimeSpan();
        }

        #endregion Method Supports
    }
}
