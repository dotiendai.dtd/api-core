﻿using Hinnova.QLNS.Dtos;
using Hinnova.Web.Chat.SignalR;
using Hinnova.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace Hinnova.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AnnouncementController : HinnovaControllerBase
    {
        private readonly IHubContext<ChatHubMobile> _chatMobileHubContext;

        public AnnouncementController(IHubContext<ChatHubMobile> chatMobileHubContext)
        {
            _chatMobileHubContext = chatMobileHubContext;
        }

        #region Mobile

        [HttpPost]
        public async Task<IActionResult> SendMessageToClient([FromBody] SendUserDto userReceiveMessages)
        {
            try
            {
                foreach (var userReceiveMessage in userReceiveMessages.AnnouncementUserDtos)
                {
                    await _chatMobileHubContext.Clients.User(userReceiveMessage.UserId.ToString())
                        .SendAsync("ReceiveMessage", userReceiveMessages.AnnouncementDtos.Find(x => x.Id.Equals(userReceiveMessage.AnnouncementId)));
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Warn("Internal Server Error");
                Logger.Warn("Could not send chat message to user");
                Logger.Warn(ex.ToString(), ex);
                return BadRequest(new ApiBadRequestResponse(ex.Message));
            }
        }

        #endregion Mobile
    }
}