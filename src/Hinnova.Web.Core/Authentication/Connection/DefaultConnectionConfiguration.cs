﻿namespace Hinnova.Web.Authentication.Connection
{
    public class DefaultConnectionConfiguration
    {
        public string Connection { get; set; }
    }
}
