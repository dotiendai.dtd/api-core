﻿using System;
using System.IO;
using System.Text;
using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.AspNetCore.SignalR;
//using Abp.AspNetZeroCore.Licensing;
using Abp.AspNetZeroCore.Web;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.IO;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Runtime.Caching.Redis;
using Abp.Zero.Configuration;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Hinnova.Chat;
using Hinnova.Configuration;
using Hinnova.EntityFrameworkCore;
using Hinnova.Startup;
using Hinnova.Web.Authentication.Connection;
using Hinnova.Web.Authentication.Email;
using Hinnova.Web.Authentication.JwtBearer;
using Hinnova.Web.Authentication.LDap;
using Hinnova.Web.Authentication.TwoFactor;
using Hinnova.Web.Chat.SignalR;
using Hinnova.Web.Configuration;
using Hinnova.Web.DashboardCustomization;

namespace Hinnova.Web
{
    [DependsOn(
        typeof(HinnovaApplicationModule),
        typeof(HinnovaEntityFrameworkCoreModule),
        typeof(AbpAspNetZeroCoreWebModule),
        typeof(AbpAspNetCoreSignalRModule),
        typeof(HinnovaGraphQLModule),
        typeof(AbpRedisCacheModule), //AbpRedisCacheModule dependency (and Abp.RedisCache nuget package) can be removed if not using Redis cache
        typeof(AbpHangfireAspNetCoreModule) //AbpHangfireModule dependency (and Abp.Hangfire.AspNetCore nuget package) can be removed if not using Hangfire
    )]
    public class HinnovaWebCoreModule : AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public HinnovaWebCoreModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            //Set default connection string
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                HinnovaConsts.ConnectionStringName
            );

            //Use database for language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                    typeof(HinnovaApplicationModule).GetAssembly()
                );
           // ConfigurationConnection();
            Configuration.Caching.Configure(TwoFactorCodeCacheItem.CacheName, cache =>
            {
                cache.DefaultAbsoluteExpireTime = TimeSpan.FromMinutes(2);
            });
            if (_appConfiguration["Authentication:JwtBearer:IsEnabled"] != null && bool.Parse(_appConfiguration["Authentication:JwtBearer:IsEnabled"]))
            {
                ConfigureTokenAuth();
                ConfigurationLDapAuth();
                ConfigurationMailAuth();
            }

            IocManager.Register<DashboardViewConfiguration>();

            Configuration.ReplaceService<IAppConfigurationAccessor, AppConfigurationAccessor>();

            //Uncomment this line to use Hangfire instead of default background job manager (remember also to uncomment related lines in Startup.cs file(s)).
            //Configuration.BackgroundJobs.UseHangfire();

            //Uncomment this line to use Redis cache instead of in-memory cache.
            //See app.config for Redis configuration and connection string
            //Configuration.Caching.UseRedis(options =>
            //{
            //    options.ConnectionString = _appConfiguration["Abp:RedisCache:ConnectionString"];
            //    options.DatabaseId = _appConfiguration.GetValue<int>("Abp:RedisCache:DatabaseId");
            //});
            // Config LDap
         
        }

        private void ConfigurationConnection()
        {
            IocManager.Register<DefaultConnectionConfiguration>();
            IocManager.Resolve<DefaultConnectionConfiguration>().Connection =
                _appConfiguration["ConnectionStrings:Default"];
        }

        private void ConfigureTokenAuth()
        {
            IocManager.Register<TokenAuthConfiguration>();
            var tokenAuthConfig = IocManager.Resolve<TokenAuthConfiguration>();
            tokenAuthConfig.SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appConfiguration["Authentication:JwtBearer:SecurityKey"]));
            tokenAuthConfig.Issuer = _appConfiguration["Authentication:JwtBearer:Issuer"];
            tokenAuthConfig.Audience = _appConfiguration["Authentication:JwtBearer:Audience"];
            tokenAuthConfig.SigningCredentials = new SigningCredentials(tokenAuthConfig.SecurityKey, SecurityAlgorithms.HmacSha256);
            tokenAuthConfig.AccessTokenExpiration = AppConsts.AccessTokenExpiration;
            tokenAuthConfig.RefreshTokenExpiration = AppConsts.RefreshTokenExpiration;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(HinnovaWebCoreModule).GetAssembly());
        }
        private void ConfigurationLDapAuth()
        {
            IocManager.Register<LDapAuthConfiguration>();
            var ldapAuthConfig = IocManager.Resolve<LDapAuthConfiguration>();
            ldapAuthConfig.Domain = _appConfiguration["Authentication:LdapAuth:Domain"];
            ldapAuthConfig.DistinguishedName = _appConfiguration["Authentication:LdapAuth:Dn"];
            ldapAuthConfig.Port = int.Parse(_appConfiguration["Authentication:LdapAuth:Port"]);
            ldapAuthConfig.SearchFilter = _appConfiguration["Authentication:LdapAuth:SearchFilter"];
            ldapAuthConfig.SearchBase = _appConfiguration["Authentication:LdapAuth:SearchBase"];
        }
        public override void PostInitialize()
        {
            SetAppFolders();
        }
        private void ConfigurationMailAuth()
        {
            IocManager.Register<EmailAuthConfiguration>();
            var emailAuthConfig = IocManager.Resolve<EmailAuthConfiguration>();
            emailAuthConfig.Server = _appConfiguration["Authentication:Settings:Server"];
            emailAuthConfig.UserName = _appConfiguration["Authentication:Settings:UserName"];
            emailAuthConfig.Password = _appConfiguration["Authentication:Settings:Password"];
            emailAuthConfig.FromEmail = _appConfiguration["Authentication:Settings:FromEmail"];
            emailAuthConfig.FromName = _appConfiguration["Authentication:Settings:FromName"];
            emailAuthConfig.Port = int.Parse(_appConfiguration["Authentication:Settings:Port"]);
            emailAuthConfig.EnableSsl = bool.Parse(_appConfiguration["Authentication:Settings:EnableSsl"]);
        }
        private void SetAppFolders()
        {
            var appFolders = IocManager.Resolve<AppFolders>();

            appFolders.SampleProfileImagesFolder = Path.Combine(_env.WebRootPath, $"Common{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}SampleProfilePics");
            appFolders.WebLogsFolder = Path.Combine(_env.ContentRootPath, $"App_Data{Path.DirectorySeparatorChar}Logs");
        }
    }
}
