﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.AspNetCore.SignalR.Hubs;
using Abp.Auditing;
using Abp.Dependency;
using Abp.RealTime;
using Abp.Runtime.Session;
using Abp.UI;
using Castle.Core.Logging;
using Castle.Windsor;
using Hinnova.QLNS.Dtos;
using Microsoft.AspNetCore.SignalR;

namespace Hinnova.Web.Chat.SignalR
{
    public class ChatHubMobile : AbpHubBase, ITransientDependency
    {
        public ChatHubMobile()
        {
            Logger = NullLogger.Instance;
            AbpSession = NullAbpSession.Instance;
        }
        public async Task SendMessage(AnnouncementDto message)
        {
            try
            {
                await Clients.All.SendAsync("ReceiveMessage", AbpSession.UserId, message);
            }
            catch (UserFriendlyException ex)
            {
                Logger.Warn("Could not send chat message to user");
                Logger.Warn(ex.ToString(), ex);
            }
            catch (Exception ex)
            {
                Logger.Warn("Internal Server Error");
                Logger.Warn("Could not send chat message to user");
                Logger.Warn(ex.ToString(), ex);
            }
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
            Logger.Warn("A client connected to ChatHubMobile: " + Context.ConnectionId);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);
            Logger.Warn("A client disconnected from ChatHubMobile: " + Context.ConnectionId);
        }
    }
}
