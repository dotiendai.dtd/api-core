﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace Hinnova.Utils
{
    public interface IViewRenderAppService : IApplicationService
    {
        Task<string> RenderViewAsync(string name, object objModel);
    }
}