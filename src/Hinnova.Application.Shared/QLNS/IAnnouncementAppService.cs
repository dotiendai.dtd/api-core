﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Hinnova.QLNS.Dtos;

namespace Hinnova.QLNS
{
    public interface IAnnouncementAppService : IApplicationService
    {
        Task<PagedResult<AnnouncementDto>> GetAllUnReadPaging(long userId, int pageIndex, int pageSize);

        Task<List<AnnouncementDto>> GetAllUnRead(long userId);

        Task<bool> MarkAsRead(List<AnnouncementUserDto> lstAnnouncementUserDtos);

        Task<AnnouncementDto> GetDetailAnnouncement(Guid id, long receiveId);

    }
}