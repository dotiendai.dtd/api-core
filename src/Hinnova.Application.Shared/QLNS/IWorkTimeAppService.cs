﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Hinnova.QLNS.Dtos;

namespace Hinnova.QLNS
{
    public interface IWorkTimeAppService : IApplicationService
    {

        Task<HrWorkTimeDto> GetWorkTimeDetail(int id, string receiveId);

        Task<List<HrWorkTimeDto>> GetWorkTimeUnCheck(string userId);

        Task<ListDangKyInputMobileDto> GetQuyTrinhDuyet(int branchId);
    }
}
