﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hinnova.QLNS.Dtos
{
    public class HrBranchDto
    {
        public int Id { get; set; }

        public int FatherId { get; set; }

        public string BranchCode { get; set; }

        public string BranchName { get; set; }

        public string BranchType { get; set; }
    }
}
