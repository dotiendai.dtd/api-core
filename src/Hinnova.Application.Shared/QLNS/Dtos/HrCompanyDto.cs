﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hinnova.QLNS.Dtos
{
    public class HrCompanyDto
    {
        public string Id { get; set; }
        
        public string CompanyName { get; set; }

        public bool Active { get; set; }
    }
}
