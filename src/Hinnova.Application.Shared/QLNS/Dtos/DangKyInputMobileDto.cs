﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hinnova.QLNS.Dtos
{
    public class DangKyInputMobileDto
    {
        public string Id { get; set; }
        public string MaChamCong { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
    }
}
