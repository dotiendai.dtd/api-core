﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS.Dtos
{
    public class HrUserGroupDto : Entity<string>
    {
        public override string Id
        {
            get => UserId + "@" + GroupId;
            set { }
        }

        public long UserId { get; set; }
        public string GroupId { get; set; }
    }
}
