﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hinnova.QLNS.Dtos
{
    public class HrDepartmentDto
    {
        public string Id { get; set; }

        public string CompanyId { get; set; }

        public string DepartmentName { get; set; }

        public bool Active { get; set; }
    }
}
