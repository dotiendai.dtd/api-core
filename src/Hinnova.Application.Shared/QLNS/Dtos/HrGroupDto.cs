﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hinnova.QLNS.Dtos
{
    public class HrGroupDto
    {
        public string Id { get; set; }

        public string DepartmentId { get; set; }

        public string CompanyId { get; set; }

        public string GroupName { get; set; }

        public bool Active { get; set; }
    }
}
