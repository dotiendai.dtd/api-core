﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hinnova.QLNS.Dtos
{
    public class ListDangKyInputMobileDto
    {
        public ListDangKyInputMobileDto()
        {
            TruongNhom = new DangKyInputMobileDto();
            TruongPhong = new DangKyInputMobileDto();
            GiamDocKhoi = new DangKyInputMobileDto();
            Tcns = new DangKyInputMobileDto();
            GiamDocDieuHanh = new DangKyInputMobileDto();
        }
        public ListDangKyInputMobileDto(DangKyInputMobileDto truongNhom, DangKyInputMobileDto truongPhong, DangKyInputMobileDto giamDocKhoi, DangKyInputMobileDto tcns, DangKyInputMobileDto giamDocDieuHanh)
        {
            TruongNhom = truongNhom;
            TruongPhong = truongPhong;
            GiamDocKhoi = giamDocKhoi;
            Tcns = tcns;
            GiamDocDieuHanh = giamDocDieuHanh;
        }

        public DangKyInputMobileDto TruongNhom { get; set; }

        public DangKyInputMobileDto TruongPhong { get; set; }

        public DangKyInputMobileDto GiamDocKhoi { get; set; }

        public DangKyInputMobileDto Tcns { get; set; }

        public DangKyInputMobileDto GiamDocDieuHanh{ get; set; }

    }
}
