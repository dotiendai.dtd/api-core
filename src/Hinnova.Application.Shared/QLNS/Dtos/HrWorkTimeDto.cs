﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities.Auditing;
using Microsoft.AspNetCore.Http;

namespace Hinnova.QLNS.Dtos
{
    public class HrWorkTimeDto : FullAuditedEntity
    {
        public string TenCty { get; set; }

        public string HoVaTen { get; set; }

        public string Image { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string DocumentType { get; set; }

        public string TimeFrom { get; set; }

        public string TimeTo { get; set; }

        public double? TotalMinutes { get; set; }

        public string Reasons { get; set; }

        public string Notes { get; set; }

        public string Attachment { get; set; }

        public short Status { get; set; }

        public long? ApproverId { get; set; }

        public long? NextApproverId { get; set; }

        public DateTime? ApproveTime { get; set; }

        public long? TruongNhomId { get; set; }

        public long? TruongPhongId { get; set; }

        public long? GiamDocKhoiId { get; set; }

        public long? TcnsId { get; set; }

        public long? GiamDocDieuHanhId { get; set; }
    }
}
