﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;

namespace Hinnova.QLNS.Dtos
{
    public class HrUserDepartmentDto : Entity<string>
    {
        public override string Id
        {
            get => UserId + "@" + DepartmentId;
            set { }
        }
        public long UserId { get; set; }

        public string DepartmentId { get; set; }
    }
}
