﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
namespace Hinnova.QLNS.Dtos
{
    public class CreateOrEditMobileDataChamCongDto : EntityDto<int>
    {
        public int MaChamCong { get; set; }
        public string CheckTime { get; set; }
        public DateTime ProcessDate { get; set; }
    }
}
