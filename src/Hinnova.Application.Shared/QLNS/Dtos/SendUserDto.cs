﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hinnova.QLNS.Dtos
{
    public class SendUserDto
    {
        public SendUserDto()
        {
            AnnouncementDtos = new List<AnnouncementDto>();
            AnnouncementUserDtos = new List<AnnouncementUserDto>();
        }

        public SendUserDto(List<AnnouncementDto> announcementDtos, List<AnnouncementUserDto> announcementUserDtos)
        {
            AnnouncementUserDtos = announcementUserDtos;
            AnnouncementDtos = announcementDtos;
        }
        public List<AnnouncementDto> AnnouncementDtos { get; set; }
        public List<AnnouncementUserDto> AnnouncementUserDtos { get; set; }
    }
}
