﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Hinnova.QLNS.Dtos
{
    public class CreateOrEditQuyTrinhCongTacDto : EntityDto<int?>
    {
        public string TenCty { get; set; }

        public string HoVaTen { get; set; }

        public string Image { get; set; }

        public string Email { get; set; }

		public string DateTo { get; set; }
        
		public string DateFrom { get; set; }

        public string ViTriCongViecCode { get; set; }

		public string QuanLyTrucTiep { get; set; }

		public int? DonViCongTacID { get; set; }

        public string TrangThaiCode { get; set; }
        
		public string GhiChu { get; set; }

        public string LyDo { get; set; }

		public string MaHoSo { get; set; }

        public DateTime NgayDuyet { get; set; }

        public string NguoiDuyetId { get; set; }
        
        public long CreatorUserId { get; set; }

        public string TruongNhomId { get; set; }

		public string TruongBoPhanId { get; set; }

        public string GiamDocBoPhanId { get; set; }

        public string PhongCTNSId { get; set; }

        public string GiamDocId { get; set; }

        public string Status { get; set; }
    }
}